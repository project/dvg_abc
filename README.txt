CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Implementation as a base theme
 * FAQ
 * Maintainers


INTRODUCTION
------------

The DVG ABC theme is a base theme that can be used with the 'Drupal voor Gemeenten' distribution.
It handles basic styling, font usage and has some great alterations for WCAG 2.0 compatiblity.

Most of the code originates from the default dvg theme,
but is almost reduced with 50% and easier to change or overwrite.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/dvg_abc

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/dvg_abc


REQUIREMENTS
------------

This theme requires the following distro:

 * DVG (https://drupal.org/project/dvg)

Any other Drupal distro or vanilla Drupal install could work, but wasn't tested.


RECOMMENDED MODULES
-------------------

Any back-end theme for creating the perfect back-end environment.

 * Seven (Drupal Core)
 * Adminimal Theme (https://www.drupal.org/project/adminimal_theme):
 * Etc..


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal theme. See:
   https://www.drupal.org/getting-started/install-contrib/themes
   for further information.

 * (Recomended) Disable DVG theme, DO NOT use 'drush dis dvg' because this will conflict with DVG profile.


CONFIGURATION
-------------

There are currently no configuration options.


IMPLEMENTATION AS A BASE THEME
------------------------------

This theme can function as a base theme for your custom DVG install. This can be
done by creating a new custom theme and define 'dvg_abc' as the 'base' theme in the info-file.


FAQ
---

Q: Where is the option to choose your custom 'head title' like the DVG theme?

A: This can be created with the Metatag module (https://drupal.org/project/metatag)
   where you can choose your head title for every entity, view or global.

Q: Where are the favicons and tiles?

A: This can be created with the Metatag module (https://drupal.org/project/metatag)
   the latest version supports many types of favicons, titles and browser styles.

Q: Is this theme fully compatible with WCAG 2.0?

A: No, but it tries to be as close as possible.

Q: Will this theme support WAI-ARIA?

A: Yes, any patch to add this feature is welcome in the issue queue:
   https://drupal.org/project/issues/dvg_abc

Q: Why is this theme not combined with project DVG?

A: DVG has it's own release path and to speedup the progress a dedicated project was the best option for now.


MAINTAINERS
-----------

Current maintainers:
 * Tessa Bakker (Tessa Bakker) - https://drupal.org/user/592104
 * Bernard Skibinski (bskibinski) - https://www.drupal.org/user/807452
