<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<?php // region: top ?>
<?php if ($html = render($page['top'])): ?>
  <div class="region r-top" id="top">
    <div class="inner">
      <nav id="nav-top">
        <?php print $html; ?>
      </nav>
    </div>
  </div>
<?php endif; ?>

<?php // region: header ?>
<header class="region r-header" id="header">
  <div class="inner">
    <?php if (!empty($logo)): ?>
      <div class="block-logo">
        <?php if (drupal_is_front_page()): ?>
          <h1 class="element-invisible"><?php print check_plain($site_name); ?></h1>
        <?php endif; ?>
        <a href="<?php print url('<front>'); ?>"><img src="<?php print $svg_logo; ?>" onerror="this.src='<?php print $logo; ?>'" alt="<?php print $site_name;?>"/></a>
      </div>
    <?php endif; ?>

    <?php if (!empty($site_slogan)): ?>
      <div class="slogan"><?php print $site_slogan; ?></div>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </div>
</header>

<?php // dvg_popup ?>
<?php if (!empty($page['popup'])): ?>
  <?php print render($page['popup']); ?>
<?php endif; ?>

<?php // system: breadcrumb ?>
<?php if ($html = render($breadcrumb)): ?>
  <nav class="region r-breadcrumbs" id="breadcrumbs">
    <div class="inner">
      <div class="wrapper-breadcrumb">
        <?php print $html; ?>
      </div>
    </div>
  </nav>
<?php endif; ?>

<?php // region: navigation ?>
<?php if ($html = render($page['navigation'])): ?>
  <nav class="region r-navigation" id="navigation">
    <div class="inner">
      <?php print $html; ?>
    </div>
  </nav>
<?php endif; ?>

<?php // region: content_top ?>
<?php if ($html = render($page['content_top'])): ?>
  <section class="region r-content-top clearfix" id="content-top">
    <div class="inner">
      <?php print $html; ?>
    </div>
  </section>
<?php endif; ?>

<?php // drupal tabs ?>
<?php if ($html = render($tabs)): ?>
  <div class="region r-tabs">
    <div class="inner">
      <?php print $html; ?>
    </div>
  </div>
<?php endif; ?>

<?php // page title ?>
<?php if ($title): ?>
  <section class="region r-page-title" id="r-page-title">
    <div class="inner">
      <?php print render($title_prefix); ?>
      <h1 class="page-title"><?php print $title; ?></h1>
      <?php print render($title_suffix); ?>
    </div>
  </section>
<?php endif; ?>

<?php // region: above_content ?>
<?php if ($html = render($page['above_content'])): ?>
  <section class="region r-above-content" id="above-content">
    <div class="inner">
      <?php print $html; ?>
    </div>
  </section>
<?php endif; ?>

<?php // system: messages ?>
<?php if ($html = render($messages)): ?>
  <section class="region r-messages" id="messages">
    <div class="inner">
      <?php print $html; ?>
    </div>
  </section>
<?php endif; ?>

<?php // region: content ?>
<main class="region r-content" id="content">
  <div class="inner">
    <?php print render($page['content']); ?>
  </div><?php // .inner ?>
</main><?php // #content ?>

<?php // region: below_content ?>
<?php if ($html = render($page['below_content'])): ?>
  <section class="region r-below-content" id="below-content">
    <div class="inner">
      <?php print $html; ?>
    </div>
  </section>
<?php endif; ?>

<?php // region: content_bottom ?>
<?php if($html = render($page['content_bottom'])): ?>
  <section class="region r-content-bottom" id="content-bottom">
    <div class="inner">
      <?php print $html; ?>
    </div>
  </section>
<?php endif; ?>

<?php // region: footer ?>
<footer class="layout l-footer" id="footer">

  <?php // region: footer_top ?>
  <?php if ($html = render($page['footer_top'])): ?>
    <div class="region r-footer-top" id="footer-top">
      <div class="inner">
        <?php print $html; ?>
      </div>
    </div>
  <?php endif; ?>

  <?php // region: footer_bottom ?>
  <?php if ($html = render($page['footer_bottom'])): ?>
    <div class="region r-footer-bottom" id="footer-bottom">
      <div class="inner">
        <?php print $html; ?>
      </div>
    </div>
  <?php endif; ?>

</footer>
