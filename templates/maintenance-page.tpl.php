<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!doctype html>
<!--[if IE 9]> <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="no-js ie ie9"><![endif]-->
<!--[if !IE]>--><html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"><!--<![endif]-->

<head>
  <?php print $head; ?>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title><?php print $head_title; ?></title>
  <link rel="apple-touch-icon" sizes="180x180" href="/<?php echo drupal_get_path('theme',  variable_get('theme_default', NULL)); ?>/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="/<?php echo drupal_get_path('theme',  variable_get('theme_default', NULL)); ?>/images/favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/<?php echo drupal_get_path('theme',  variable_get('theme_default', NULL)); ?>/images/favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/<?php echo drupal_get_path('theme',  variable_get('theme_default', NULL)); ?>/images/favicon/manifest.json">
  <link rel="mask-icon" href="/<?php echo drupal_get_path('theme',  variable_get('theme_default', NULL)); ?>/images/favicon/safari-pinned-tab.svg" color="#456479">
  <link rel="shortcut icon" href="/<?php echo drupal_get_path('theme',  variable_get('theme_default', NULL)); ?>/images/favicon/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="DVG">
  <meta name="application-name" content="DVG">
  <meta name="msapplication-config" content="/<?php echo drupal_get_path('theme',  variable_get('theme_default', NULL)); ?>/images/favicon/browserconfig.xml">
  <meta name="theme-color" content="#456479">
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body class="<?php print $classes; ?>" <?php print $attributes;?>>

  <?php // region: header ?>
  <header class="region r-header" id="header">
    <div class="inner">
      <?php if (!empty($logo)): ?>
        <div class="logo">
          <?php if (drupal_is_front_page()): ?>
            <h1 class="element-invisible"><?php print check_plain($site_name); ?></h1>
          <?php endif; ?>
          <a href="<?php print url('<front>'); ?>"><img src="<?php print $svg_logo; ?>" onerror="this.src='<?php print $logo; ?>'" alt="<?php print $site_name;?>"/></a>
        </div>
      <?php endif; ?>

      <?php if (!empty($site_slogan)): ?>
        <div class="slogan"><?php print $site_slogan; ?></div>
      <?php endif; ?>
    </div>
  </header>

  <?php // page title ?>
  <?php if ($title): ?>
    <section class="region r-page-title" id="r-page-title">
      <div class="inner">
        <?php print render($title_prefix); ?>
        <h1 class="page-title"><?php print $title; ?></h1>
        <?php print render($title_suffix); ?>
      </div>
    </section>
  <?php endif; ?>

  <?php // system: messages ?>
  <?php if ($html = render($messages)): ?>
    <section class="region r-messages" id="messages">
      <div class="inner">
        <?php print $html; ?>
      </div>
    </section>
  <?php endif; ?>

  <?php // region: content ?>
  <main class="region r-content" id="content">
    <div class="inner">
      <?php print render($page['content']); ?>
    </div><?php // .inner ?>
  </main><?php // #content ?>

  <?php // region: footer ?>
  <footer class="layout l-footer" id="footer">

    <?php // region: footer_top ?>
      <div class="region r-footer-top" id="footer-top">
        <div class="inner">
        </div>
      </div>

    <?php // region: footer_bottom ?>
      <div class="region r-footer-bottom" id="footer-bottom">
        <div class="inner">
        </div>
      </div>
  </footer>

</body>
</html>