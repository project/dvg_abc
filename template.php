<?php

/**
 * Implements theme_css_alter().
 *
 * - Remove **some** Drupal CSS files so we can implement that ourselves (better!).
 */
function dvg_abc_css_alter(&$css) {
  $remove = array(
    'user',
    'image',
    'system.base',
    'system.theme',
    'system.messages',
    'system.menus',
    'contextual',
    'node',
    'views',
    'field',
    'ctools',
    'taxonomy',
    'field_collection.theme',
  );
  dvg_abc_remove_css($css, $remove);
}

/**
 * Helper to remove CSS files from a CSS file list.
 *
 * You can use this function in your subtheme's hook_css_alter().
 *
 * NOTE the `$remove` argument is an array of stylesheet basenames **without** `.css`.
 */
function dvg_abc_remove_css(&$css, $remove) {
  foreach ($css as $file => $meta) {
    if (in_array(substr(basename($file), 0, -4), $remove)) {
      unset($css[$file]);
    }
  }
}

/**
 * Implements hook_js_alter().
 *
 * Unset 'split the menu page lists in two'.
 * Unset 'tab reindex'.
 */
function dvg_abc_js_alter(&$js) {
  unset(
    $js[drupal_get_path('module', 'dvg_ct_menu_page') . '/dvg_ct_menu_page_menu_block.js']
  );
}

/**
 * Implements theme_status_messages().
 *
 * Make sure `.messages > *` is **always** an UL, even if there's only 1 message.
 *
 * @see http://api.drupal.org/api/drupal/includes!theme.inc/function/theme_status_messages/7
 */
function dvg_abc_status_messages($display = NULL) {
  $all_messages = drupal_get_messages();

  $output = '';
  foreach ($all_messages as $type => $messages) {
    if ($messages) {
      $output .= '<div class="messages ' . $type . '">' . "\n";
      $output .= '<ul class="menu">' . "\n";
      foreach ($messages as $message) {
        $output .= '<li>' . $message . "</li>\n";
      }
      $output .= "</ul>\n";
      $output .= "</div>\n";
    }
  }

  return $output;
}

/**
 * Implements template_preprocess_maintenance_page().
 */
function dvg_abc_preprocess_maintenance_page(&$variables) {
  global $theme;

  // SVG Logo.
  $variables['svg_logo'] = base_path() . drupal_get_path('theme', $theme) . '/images/favicon/logo.svg';
}

/**
 * Implements template_preprocess_html().
 */
function dvg_abc_preprocess_html(&$variables) {
  // Remove Drupal's "no-sidebars" class.
  if ($key = array_search('no-sidebars', $variables['classes_array'])) {
    unset($variables['classes_array'][$key]);
  }

  // Add body classes for active contexts.
  if (module_exists('context')) {
    $contexts = context_active_contexts();
    foreach ($contexts as $c_id => $c) {
      $variables['classes_array'][] = 'context-' . $c_id;
    }
  }

  // `with` / `without` classes for certain regions
  foreach (array('sidebar_left', 'sidebar_right') as $region_name) {
    $without = empty($variables['page'][$region_name]);
    $variables['classes_array'][] = ($without ? 'without' : 'with') . '-' . str_replace('_', '-', $region_name);
  }

  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_left']) && !empty($variables['page']['sidebar_right'])) {
    $variables['classes_array'][] = 'all-sidebars';
  }
  elseif (empty($variables['page']['sidebar_left']) && empty($variables['page']['sidebar_right'])) {
    $variables['classes_array'][] = 'no-sidebars';
  }
}

/**
 * Implements template_preprocess_page().
 */
function dvg_abc_preprocess_page(&$variables) {
  global $theme;

  $variables['is_node'] = 'node' == arg(0) && is_numeric(arg(1));

  $search_page_nid = functional_content_nid(_functional_content_item_name('search', 'block'));
  if (arg(0) == 'node' && arg(1) == $search_page_nid && !arg(2) && !empty($_GET['search'])) {
    $variables['title'] = t('Search results for <span>@term</span>', array('@term' => $_GET['search']));
  }

  // SVG Logo.
  $variables['svg_logo'] = base_path() . drupal_get_path('theme', $theme) . '/images/favicon/logo.svg';
}

/**
 * Implements template_preprocess_node().
 */
function dvg_abc_preprocess_node(&$variables) {

  // Allow to use the anchor on frontpage nodes.
  $is_front = $variables['view_mode'] == 'frontpage';
  if ($is_front) {
    $path = drupal_get_path_alias('node/' . $variables['nid']);
    $variables['html_id'] = drupal_html_id($path);
  }

  $node = $variables['node'];

  // Add template suggestions.
  $variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $variables['view_mode'];

  // Adds same CSS class.
  $variables['classes_array'][] = drupal_clean_css_identifier(implode('-', array(
    'node',
    $node->type,
    $variables['view_mode']
  )));

  // We do our own theming in the node template.
  unset($variables['content']['field_highlight_text']['#theme']);

  // Nodes on non-pages have a linkable H2 title
  if (!isset($variables['page_link'])) {
    $variables['page_link'] = TRUE;
  }
}

/**
 * Implements theme_breadcrumb().
 */
function dvg_abc_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (empty($breadcrumb)) {
    return;
  }

  $new_breadcrumb = array();
  $new_breadcrumb[] = '<nav class="breadcrumb" role="navigation" aria-label="' . t('You are here') . ':">';

  foreach($breadcrumb as $key => $crumb) {
    if ($key) {
      if (preg_match('/href="(?:.*)#(?:.*)">/', $crumb)) {
        $new_breadcrumb[] = ' <span class="breadcrumb-symbol breadcrumb-symbol-front">:</span> ';
      }
      else {
        $new_breadcrumb[] = ' <span class="breadcrumb-symbol">&gt;</span> ';
      }
    }

    $new_breadcrumb[] = $crumb;
  }

  return implode('', $new_breadcrumb) . '</div>';
}

/**
 * Definition of functional content menus.
 */
function _dvg_abc_breadcrumb_functional_content_menus() {
  $config = array(
    'menu-about' => 'dvg_global__front_about__nid',
    'menu-organization' => 'dvg_global__front_organization__nid',
  );

  drupal_alter('dvg_abc_functional_content_menus', $config);

  return $config;
}

/**
 * Implements hook_menu_breadcrumb_alter().
 *
 * Insert Menu name (functional content node title) after 'Home'.
 */
function dvg_abc_menu_breadcrumb_alter(&$active_trail, $item) {
  $menu_trail = &drupal_static(__FUNCTION__);
  $menu_name = NULL;
  $new_active_trail = array();

  foreach ($active_trail as $key => $trail) {

    // Skip if no menu name.
    if (empty($trail['menu_name'])) {
      $new_active_trail[] = $trail;
      continue;
    }

    $menu_name = $trail['menu_name'];
    $menus = _dvg_abc_breadcrumb_functional_content_menus();

    // Skip if no menu name has been processed or menu is not a functional content menu.
    if (isset($menu_trail[$menu_name]) || !isset($menus[$menu_name])) {
      $new_active_trail[] = $trail;
      continue;
    }

    // Insert extra breadcrumb item.
    $menu_node = functional_content_node($menus[$menu_name]);
    if ($menu_node) {
      $menu_trail[$menu_name] = $menu_node;

      $path = drupal_get_path_alias('node/' . $menu_node->nid);

      $new_active_trail[] = array(
        'title' => $menu_node->title,
        'link_path' => '<front>',
        'href' => '<front>',
        'localized_options' => array(
          'fragment' => drupal_html_id($path),
        ),
      );
    }

    $new_active_trail[] = $trail;
  }

  // Rebuild breadcrumb.
  $active_trail = $new_active_trail;
}

/**
 * Implements theme_facetapi_link_active().
 *
 * @see theme_facetapi_link_inactive().
 *
 * Makes sure the active link "looks" identical to the inactive link.
 */
function dvg_abc_facetapi_link_active($variables) {
  // Builds accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => TRUE,
  );
  $accessible_markup = theme('facetapi_accessible_markup', $accessible_vars);

  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $variables['text'] = ($sanitize) ? check_plain($variables['text']) : $variables['text'];

  // Adds count to link if one was passed.
  if (isset($variables['count'])) {
    $variables['text'] .= ' ' . theme('facetapi_count', $variables);
  }

  // Resets link text, sets to options to HTML since we already sanitized the
  // link text and are providing additional markup for accessibility.
  $variables['text'] .= $accessible_markup;
  $variables['options']['html'] = TRUE;
  return theme_link($variables);
}

/**
 * Implements theme_webform_progressbar().
 */
function dvg_abc_webform_progressbar($vars) {
  if (isset($vars['progressbar_bar']) && !$vars['progressbar_bar']) {
    return '';
  }

  $vars['classes_array'][] = 'progressbar';
  $items = array();
  foreach ($vars['page_labels'] as $index => $label) {
    $classes = array();
    $replacements = array(
      '@step' => ($index + 1),
      '@total' => $vars['page_count']
    );

    if ($index < ($vars['page_num'] - 1)) {
      $accessibility = t('Step @step of @total. Completed step', $replacements);
      $classes[] = 'completed';
    }
    elseif ($index == ($vars['page_num'] - 1)) {
      $accessibility = t('Step @step of @total. Active step', $replacements);
      $classes[] = 'current';
    }
    else {
      $accessibility = t('Step @step of @total', $replacements);
    }

    $items[] = array(
      'class' => $classes,
      'data' => '<span class="label">' . t('<span class="element-invisible">!accessibility: </span>@label', array(
          '@label' => $label,
          '!accessibility' => $accessibility,
        )) . '</span>',
    );
  }

  return theme('item_list', array(
    'items' => $items,
    'type' => 'ol',
    'attributes' => array(
      'class' => $vars['classes_array'],
    ),
  ));
}

/**
 * Implements theme_form_required_marker().
 */
function dvg_abc_form_required_marker($variables) {
  return '';
}

/**
 * Search form input alter.
 */
function dvg_abc_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#id'] == 'views-exposed-form-search-page') {
    $form['search']['#attributes']['placeholder'] = t('Search');
  }
}

/**
 * Implements hook_entity_view_alter().
 *
 * Re-structure the headings comform W3C standaards.
 */
function dvg_abc_entity_view_alter(&$build, $type) {
  $field = array(
    'field_body' => array(
      'type' => 'field_collection_item',
      'bundle' => 'field_sections',
    ),
  );

  // Needed for empty view modes.
  if (!isset($build['#bundle'])) {
    return;
  }

  foreach ($field as $field_name => $info) {

    if ($info['type'] == $type && $info['bundle'] == $build['#bundle']
      && isset($build[$field_name])
      && is_array($build[$field_name])) {

      foreach (element_children($build[$field_name]) as $i) {
        if (!isset($build[$field_name][$i]['#markup'])) {
          continue;
        }

        if (strpos($build[$field_name][$i]['#markup'], '<h2>') !== FALSE) {
          $build[$field_name][$i]['#markup'] = str_replace(
            array('<h3>', '</h3>', '<h2>', '</h2>'),
            array('<h4>', '</h4>', '<h3>', '</h3>'),
            $build[$field_name][$i]['#markup']
          );
        }
      }
    }
  }
}
